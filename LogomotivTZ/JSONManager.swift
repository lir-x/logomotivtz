//
//  JSONManager.swift
//  LogomotivTZ
//
//  Created by Lir-x on 02.09.16.
//  Copyright © 2016 Lir-x. All rights reserved.
//

import UIKit
import SwiftyJSON

class JSONManager {
    
    static let sharedInstance = JSONManager()
    
    let file = NSBundle.mainBundle().pathForResource("data", ofType: "json") as String!
        
    func JSONFromFile() -> JSON{
        let data = NSData.init(contentsOfFile: file) as NSData!
        let json = JSON(data: data, options: .MutableContainers, error: nil)
        return json
    }
    
    func getJSONData(key:String) -> [TestProduct]!{
        
        print("getJSONData with key\(key)")
        var result: [TestProduct] = []

        let json = JSONFromFile()
        guard let jsonProduct = json["Products"].arrayValue as Array! else { return nil }

        for (index, product) in jsonProduct.enumerate() {
            
            guard let name      = product["Name"].stringValue as String!,
                let discription = product["Discription"].stringValue as String!,
                let price       = product["Price"].stringValue as String!,
                let productKey  = product["Key"].stringValue as String!
            
                else {print("retunt nil"); return nil}
            if productKey == key {
                
                result.append(TestProduct(name: name, discription: discription, price: price, key: productKey,index: index))

            }
                    }
    return result
    }
    
    func writeToFile(json:JSON)  {
        let str = json.description
        do{
            try str.writeToFile(self.file, atomically: true, encoding: NSUTF8StringEncoding)
            } catch {
        }
    }
}

