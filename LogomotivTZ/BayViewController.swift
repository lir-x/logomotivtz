//
//  BayViewController.swift
//  LogomotivTZ
//
//  Created by Lir-x on 03.09.16.
//  Copyright © 2016 Lir-x. All rights reserved.
//

import UIKit

class BayViewController: UITableViewController {
    
    var json = JSONManager.sharedInstance
    
    var productsForBay = JSONManager().getJSONData("Buy")
    override func viewDidLoad() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        productsForBay = json.getJSONData("Buy")

        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsForBay.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
        
        let product = productsForBay[indexPath.row]
        
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = product.discription
        
        return cell
        
    }
    
    
    // MARK: UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let storyboard = self.storyboard
        let vc = storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
        vc.key = "Buy"
        let product = productsForBay[indexPath.row]
        vc.product = product
        self.navigationController?.title = "Buy"
        self.navigationController?.pushViewController(vc, animated: true)

}
}