//
//  TestProduct.swift
//  LogomotivTZ
//
//  Created by Lir-x on 03.09.16.
//  Copyright © 2016 Lir-x. All rights reserved.
//

import Foundation

class  TestProduct {
    let name : String
    let discription : String
    let price : String
    let key : String
    let index : Int
    
    init(name:String, discription: String, price : String ,key: String,index:Int) {
        self.name = name
        self.discription = discription
        self.price = price
        self.key = key
        self.index = index
    }
        
}
