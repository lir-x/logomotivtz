//
//  DetailViewController.swift
//  LogomotivTZ
//
//  Created by Lir-x on 03.09.16.
//  Copyright © 2016 Lir-x. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var json = JSONManager.sharedInstance
    var product:TestProduct!
    
    var key = ""
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    var index: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = product?.name
        discriptionLabel.text = product?.discription
        priceLabel.text = "Price "+(product?.price)!

        if navigationController?.title == "Buy"{
            let addToSaleButton = UIBarButtonItem.init(title: "Add to sale",
                                                      style: UIBarButtonItemStyle.Plain,
                                                      target: self,
                                                      action: #selector(DetailViewController.barButtonAction(_:)))
            self.navigationItem.rightBarButtonItem = addToSaleButton
            key = "Sale"
            
            
        }
        if navigationController?.title ==  "Sale"{
            let addToBay = UIBarButtonItem.init(title: "Add to buy",
                                                       style: UIBarButtonItemStyle.Plain,
                                                       target: self,
                                                       action: #selector(DetailViewController.barButtonAction(_:)))
            self.navigationItem.rightBarButtonItem = addToBay
            key = "Buy"
        }
    }
    
    func barButtonAction(sender:UIBarButtonItem) {
        var j = json.JSONFromFile()
        j["Products"][product.index]["Key"].string = key
        json.writeToFile(j)
        navigationController?.popViewControllerAnimated(true)
    }
    
    
}
