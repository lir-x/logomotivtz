//
//  SaleViewController.swift
//  LogomotivTZ
//
//  Created by Lir-x on 03.09.16.
//  Copyright © 2016 Lir-x. All rights reserved.
//

import UIKit

class SaleViewConttroler: UITableViewController {
    
    var json = JSONManager.sharedInstance
    
    var productsForSale = JSONManager().getJSONData("Sale")


    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        
        productsForSale = json.getJSONData("Sale")
        tableView.reloadData()
    }
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsForSale.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
        
        let product = productsForSale[indexPath.row]
        
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = product.discription
        
        
        return cell
        
    }
        
    // MARK: UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let storyboard = self.storyboard
        let vc = storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
        vc.key = "Sale"
        let product = productsForSale[indexPath.row]
        vc.product = product
//        vc.index = indexPath.row
        self.navigationController?.title = "Sale"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
